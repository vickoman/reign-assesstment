import { Injectable, Logger } from '@nestjs/common';
import * as axios from 'axios';

@Injectable()
export class HackerNewsGateway {
    logger: Logger = new Logger('HackerNewsGateway');

    async getLatestNews(): Promise<any> {
        const config: axios.AxiosRequestConfig = {
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const url = process.env.ALGOLIA_URL;

        try {
            const res = await axios.default.get(url, config);
            return res.data.hits;
        } catch (err) {
            throw new Error(err.message);
        }
    }
}
