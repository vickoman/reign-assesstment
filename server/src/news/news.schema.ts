import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type newsDocument = News & Document;

@Schema()
export class News {
    @Prop()
    title: string;

    @Prop()
    isActive: boolean;

    @Prop()
    created_at: Date;

    @Prop()
    url: string;

    @Prop()
    author: string;

    @Prop()
    story_id: number;

    @Prop()
    story_title: string;

    @Prop()
    story_url: string;

    @Prop()
    created_at_i: number;

    @Prop()
    objectID: string;
}

export const NewsSchema = SchemaFactory.createForClass(News);
