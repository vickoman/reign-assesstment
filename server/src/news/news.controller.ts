import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
} from '@nestjs/common';
import { NewsDTO } from './news.dto';
import { NewsService } from './news.service';

@Controller('api/news')
export class NewsController {
    constructor(private newsService: NewsService) {}

    @Get()
    getAllNews() {
        return this.newsService.findAll();
    }

    @Get(':id')
    read(@Param('id') newsId: string) {
        return this.newsService.findById(newsId);
    }

    @Post()
    addNews(@Body() data: NewsDTO) {
        return this.newsService.create(data);
    }

    @Put(':id')
    updateNews(@Param('id') newsId: string, @Body() data: Partial<NewsDTO>) {
        return this.newsService.update(newsId, data);
    }

    @Delete(':id')
    deleteNews(@Param('id') newsId: string) {
        return this.newsService.destroy(newsId);
    }

    @Get('/cron')
    executwCron() {
        return this.newsService.handleCron();
    }
}
