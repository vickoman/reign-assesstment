import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNewsGateway } from '../infrastructure/gateways/hn.gateway';
import { NewsController } from './news.controller';
import { News, NewsSchema } from './news.schema';
import { NewsService } from './news.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: News.name, schema: NewsSchema }]),
    ],
    controllers: [NewsController],
    providers: [NewsService, HackerNewsGateway],
})
export class NewsModule {}
