export class NewsDTO {
    _id?: string;
    isActive?: boolean;
    created_at?: Date;
    title?: string;
    url?: string;
    author?: string;
    story_id: number;
    story_title?: string;
    story_url?: string;
    created_at_i: number;
    objectID: string;
}

export class NewsRRO extends NewsDTO {}

export interface NewsActionResponse {
    isOk: boolean;
    message: string;
    news?: NewsRRO;
}
