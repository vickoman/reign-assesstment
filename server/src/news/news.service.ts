import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { HackerNewsGateway } from '../infrastructure/gateways/hn.gateway';
import { NewsDTO, NewsActionResponse, NewsRRO } from './news.dto';
import { News, newsDocument } from './news.schema';

@Injectable()
export class NewsService {
    private readonly logger: Logger = new Logger(NewsService.name);
    constructor(
        @InjectModel(News.name) private newsModel: Model<newsDocument>,
        private hackerNewsGateway: HackerNewsGateway,
    ) {}

    async findAll(): Promise<NewsRRO[]> {
        return this.newsModel
            .find({ isActive: true })
            .sort([['created_at', -1]])
            .exec();
    }

    async findById(newsId: string): Promise<NewsRRO> {
        try {
            const notice = await this.newsModel.findById(newsId);
            return notice;
        } catch (err) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
    }

    async create(newObject: NewsDTO): Promise<NewsActionResponse> {
        try {
            const exist = await this.newsModel.findOne({
                objectID: newObject.objectID,
            });
            if (!exist) {
                const createdNews = new this.newsModel({
                    ...newObject,
                    isActive: true,
                });
                const newNotice = await createdNews.save();
                return {
                    isOk: true,
                    message: `New post was saved _id: ${newNotice._id}`,
                    news: newNotice,
                };
            } else {
                return {
                    isOk: false,
                    message: `The post with objectID: ${exist.objectID} exists`,
                    news: exist,
                };
            }
        } catch (err) {
            throw new HttpException(err.message || err.name, err.HttpStatus);
        }
    }

    async update(
        newsId: string,
        updateObject: Partial<NewsDTO>,
    ): Promise<NewsActionResponse> {
        try {
            const exist = await this.findById(newsId);
            if (!exist) {
                throw new HttpException('Not found', HttpStatus.NOT_FOUND);
            }

            try {
                await this.newsModel.updateOne(
                    { _id: newsId },
                    { $set: { ...updateObject } },
                    { upsert: true },
                );
            } catch (errUpdate) {
                throw new HttpException(
                    errUpdate.message || errUpdate.name,
                    errUpdate.HttpStatus,
                );
            }

            const notice = await this.newsModel.findOne({ _id: newsId });
            return {
                isOk: true,
                message: `${newsId} was updated`,
                news: notice,
            };
        } catch (err) {
            throw new HttpException(err.message || err.name, err.HttpStatus);
        }
    }

    async destroy(newsId: string): Promise<NewsActionResponse> {
        try {
            const exist = await this.findById(newsId);
            if (!exist) {
                throw new HttpException('Not found', HttpStatus.NOT_FOUND);
            }

            try {
                await this.newsModel.deleteOne({ _id: newsId });
            } catch (delErr) {
                throw new HttpException(
                    delErr.message || delErr.name,
                    delErr.HttpStatus,
                );
            }

            return { isOk: true, message: `${newsId} was deleted` };
        } catch (err) {
            throw new HttpException(err.message || err.name, err.HttpStatus);
        }
    }

    @Cron('0 * * * *')
    async handleCron() {
        const news = await this.hackerNewsGateway.getLatestNews();
        try {
            news.map(async (n) => {
                const {
                    created_at,
                    title,
                    url,
                    author,
                    story_id,
                    story_title,
                    story_url,
                    created_at_i,
                    objectID,
                } = n;
                const mappedObject = {
                    created_at,
                    title,
                    url,
                    author,
                    story_id,
                    story_title,
                    story_url,
                    created_at_i,
                    objectID,
                };
                this.logger.debug(
                    `${JSON.stringify(mappedObject)} will be inserted`,
                );
                await this.create(mappedObject);
            });
        } catch (err) {
            throw new HttpException(err.message || err.name, err.HttpStatus);
        }
    }
}
