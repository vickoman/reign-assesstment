import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from './news.service';

class ApiNewsMock {
    findAll() {
        return {
            _id: '5fe0295388f4dfb261fdf78a',
            created_at: '2020-12-20T22:18:51.000Z',
            title: null,
            url: null,
            author: 'reaperducer',
            story_id: 25485651,
            story_title: 'The web is 30 years old today',
            story_url: 'https://home.cern/science/computing/birth-web',
            created_at_i: 1608502731,
            objectID: '25490085',
            isActive: true,
        };
    }
}

describe('StudentService', () => {
    let app: TestingModule;
    let newsService: NewsService;

    beforeAll(async () => {
        const ApiServiceProvider = {
            provide: NewsService,
            useClass: ApiNewsMock,
        };
        app = await Test.createTestingModule({
            providers: [NewsService, ApiServiceProvider],
        }).compile();
        newsService = app.get<NewsService>(NewsService);
    });

    describe('findAll news', () => {
        it('should get All order descending in created_at', async () => {
            const expectedResponse = {
                _id: '5fe0295388f4dfb261fdf78a',
                created_at: '2020-12-20T22:18:51.000Z',
                title: null,
                url: null,
                author: 'reaperducer',
                story_id: 25485651,
                story_title: 'The web is 30 years old today',
                story_url: 'https://home.cern/science/computing/birth-web',
                created_at_i: 1608502731,
                objectID: '25490085',
                isActive: true,
            };
            const news = await newsService.findAll();
            expect(news).toEqual(expectedResponse);
        });
    });
});
