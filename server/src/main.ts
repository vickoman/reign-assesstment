import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.enableCors({
        origin: [
            'http://localhost:4200', // angular
            'http://localhost:3001', // react
            `http://localhost:${process.env.DOCKER_CLIENT_REACT_PORT}`, // react from Docker
            'http://localhost:8080', // Vue
        ],
    });
    await app.listen(process.env.PORT || 3000);
}
bootstrap();
