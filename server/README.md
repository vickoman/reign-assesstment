## Server component

This is a server component, was cread with nestjs framework and mongodb


Please create the .env file with the follow env vars copy the values:

```
ALGOLIA_URL=https://hn.algolia.com/api/v1/search_by_date?query=nodejs
DOCKER_CLIENT_REACT_PORT=8182  // Port that we want to load with docker
MONGO_URL=mongodb://localhost:27017/reign_data
PORT=8998
```

Start API:
`yarn start:dev`

 😎 Fill the first time the database 😎:
`curl -X GET http://localhost:8998/api/news/cron`