import React from 'react';

interface Props {
    title: string;
    subtitle: string;
}

const Header: React.FC<Props> = props => {
    return (
        <div className="App-header">
            <p>{props.title}</p>
            <p>{props.subtitle}</p>
        </div>
    )
}

export default Header;
