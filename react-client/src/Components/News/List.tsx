import React, { Fragment } from 'react';
import Card from './Card';

type HNew = {
    _id: string;
    isActive?: boolean;
    created_at?: Date;
    title?: string;
    url?: string;
    author?: string;
    story_id: number;
    story_title?: string;
    story_url?: string;
    created_at_i: number;
    objectID: string;
};
interface Props {
    hackerNews: HNew[]
    onRemove: Function
}
const ListNews: React.FC<Props> = props => {
    return (
        <Fragment>
            { props.hackerNews.length > 0 &&
                props.hackerNews.map((n, idx) => (
                    <Card key={n.objectID} dataNew={n} onRemove={props.onRemove} />
            ))}
            { props.hackerNews.length === 0 && 
            <div className="App-news_row">
                <span className="App-news_row_title">
                    Not news :(
                </span>
            </div>}
        </Fragment>
    );
}
export default ListNews;
