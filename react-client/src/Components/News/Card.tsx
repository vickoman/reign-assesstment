import React from 'react';
import Moment from 'react-moment';
import TrashIcon from '../Shared/TrashIcon';

interface Props {
    dataNew: {
        _id: string,
        title?: string,
        story_title?: string,
        author?: string,
        created_at?: Date,
        story_url?: string
    },
    onRemove: Function
}

const Card: React.FC<Props> = props => {
    const { title, story_title, author, created_at, story_url, _id} = props.dataNew;

    if ( story_title || title) {
        return (
                <div className="App-news_row grid_parent">
                        <div className="title">
                            <a href={story_url} target="_blank" rel="noreferrer" className="NoLink">
                                <span className="App-news_row_title">{story_title ? story_title : title}</span>
                                <span className="App-news_row_author"> - {author} - </span>
                            </a>
                        </div>
                        <div className="date">
                            <span>
                            <Moment fromNow>{created_at}</Moment>
                            </span>
                        </div>
                    <div className="action">
                        <span onClick={() => props.onRemove(_id)}>
                            <TrashIcon />
                        </span>
                    </div>
                </div>
        )
    } else {
        return <div></div>
    }
}

export default Card;