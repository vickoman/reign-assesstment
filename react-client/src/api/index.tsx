import * as axios from 'axios';

export const getLatestNews = async(): Promise<any> => {
    const url = `${process.env.REACT_APP_API_URL}/api/news`;
    const response = await genericGet(url);
    return response.data;
};

export const deleteNewsById = async(newsId: string, data: any): Promise<any> => {
    const url = `${process.env.REACT_APP_API_URL}/api/news/${newsId}`;
    const response = await genericPut(url, data);
    return response.data;
};

export const genericGet = async (url: string, extraHeaders?: { [key: string]: any }, authorization?: boolean) => {
    const config: axios.AxiosRequestConfig = {
        headers: {
            "Content-Type": "application/json",
            ...extraHeaders && { ...extraHeaders },
        }
    }
    return axios.default.get(url, config);
}

export const genericPut = async (url: string, data: any, extraHeaders?: { [key: string]: any }, authorization?: boolean) => {
    const config: axios.AxiosRequestConfig = {
        headers: {
            "Content-Type": "application/json",
            ...extraHeaders && { ...extraHeaders },
        }
    }
    return axios.default.put(url, data, config);
}