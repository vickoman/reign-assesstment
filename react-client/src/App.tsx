import React, { useEffect, useState } from 'react';
import { deleteNewsById, getLatestNews } from './api';
import './App.css';
import ListNews from './Components/News/List';
import Header from './Components/Shared/Header';
require('dotenv').config()

function App() {
  const [news, setNews] = useState([]);


  useEffect(()  => {
    (async () => {
      const response = await getLatestNews();
      setNews(response);
    })();
  }, []);

  const handleRemoveNews = async (newsId: string) => {
    const payload = { isActive: false };
    await deleteNewsById(newsId, payload);
    const newNews = news.filter((n:any) => n._id !== newsId);
    setNews(newNews);
  };

  return (
    <div className="App">
      <Header title="HN Feeds" subtitle="We <3 hacker news!" />
      <ListNews hackerNews={news} onRemove={handleRemoveNews} />
    </div>
  );
}

export default App;
