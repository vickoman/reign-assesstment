## Client Component with React

This is the client component of the test, using React with typescript ⚡️.

We have to create the  the .env file (copy the values):
```
PORT=8182
REACT_APP_API_URL=http://localhost:3000
```

For run:
`yarn start`

For build:
`yarn build`