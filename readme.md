### Technical Test Reign:

![Test assesstment Reign](project.png "Test Assesstment Reign Victor Diaz")

For run this project we have 2 ways:

First and short way is using docker-compose: `docker-compose up -d`, this command turn on our application.

Large way:

inside to the folder react-client, create the `.env` file, please read the readme file inside of each component folder:.

```
PORT=8182
REACT_APP_API_URL=http://localhost:3000
```

The same medicine for the server component: create the `.env` file with the following data:
```
ALGOLIA_URL=https://hn.algolia.com/api/v1/search_by_date?query=nodejs
DOCKER_CLIENT_REACT_PORT=8182  // Port that we want to load with docker
MONGO_URL=mongodb://localhost:27017/reign_data
PORT=8998
```

👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇
**If we need to fill the database the first time we can use the endpont:**
`curl -X GET http://localhost:8998/api/news/cron`
